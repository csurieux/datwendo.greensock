using Orchard.UI.Resources;

namespace Datwendo.Greensock {
    public class ResourceManifest : IResourceManifestProvider {
        public void BuildManifests(ResourceManifestBuilder builder) {
            var manifest = builder.Add();
            manifest.DefineScript("JqueryGSAP").SetUrl("Greensock/jquery.gsap.min.js", "Greensock/jquery.gsap.js").SetCdn("//cdnjs.cloudflare.com/ajax/libs/gsap/1.18.2/jquery.gsap.min.js",null,true).SetVersion("1.18.2").SetDependencies("jQuery");
            manifest.DefineScript("TimelineLite").SetUrl("Greensock/TimelineLite.min.js", "Greensock/TimelineLite.js").SetCdn("//cdnjs.cloudflare.com/ajax/libs/gsap/1.18.2/TimelineLite.min.js",null,true).SetVersion("1.18.2");
            manifest.DefineScript("TimelineMax").SetUrl("Greensock/TimelineMax.min.js", "Greensock/TimelineMax.js").SetCdn("//cdnjs.cloudflare.com/ajax/libs/gsap/1.18.2/TimelineMax.min.js",null,true).SetVersion("1.18.2");
            manifest.DefineScript("TweenLite").SetUrl("Greensock/TweenLite.min.js", "Greensock/TweenLite.js").SetCdn("//cdnjs.cloudflare.com/ajax/libs/gsap/1.18.2/TweenLite.min.js",null,true).SetVersion("1.18.2");
            manifest.DefineScript("TweenMax").SetUrl("Greensock/TweenMax.min.js", "Greensock/TweenMax.js").SetCdn("//cdnjs.cloudflare.com/ajax/libs/gsap/1.18.2/TweenMax.min.js",null,true).SetVersion("1.18.2");
            manifest.DefineScript("EasePack").SetUrl("Greensock/Easing/EasePack.min.js", "Greensock/Easing/EasePack.js").SetCdn("//cdnjs.cloudflare.com/ajax/libs/gsap/1.18.2/easing/EasePack.min.js",null,true).SetVersion("1.18.2");
            manifest.DefineScript("AttrPlugin").SetUrl("Greensock/Plugins/AttrPlugin.min.js", "Greensock/Plugins/AttrPlugin.js").SetCdn("//cdnjs.cloudflare.com/ajax/libs/gsap/1.18.2/plugins/AttrPlugin.min.js",null,true).SetVersion("1.18.2");
            manifest.DefineScript("BezierPlugin").SetUrl("Greensock/Plugins/BezierPlugin.min.js", "Greensock/Plugins/BezierPlugin.js").SetCdn("//cdnjs.cloudflare.com/ajax/libs/gsap/1.18.2/plugins/BezierPlugin.min.js",null,true).SetVersion("1.18.2");
            manifest.DefineScript("ColorPropsPlugin").SetUrl("Greensock/Plugins/ColorPropsPlugin.min.js", "Greensock/Plugins/ColorPropsPlugin.js").SetCdn("//cdnjs.cloudflare.com/ajax/libs/gsap/1.18.2/plugins/ColorPropsPlugin.min.js",null,true).SetVersion("1.18.2");
            manifest.DefineScript("CSSPlugin").SetUrl("Greensock/Plugins/CSSPlugin.min.js", "Greensock/Plugins/CSSPlugin.js").SetCdn("//cdnjs.cloudflare.com/ajax/libs/gsap/1.18.2/plugins/CSSPlugin.min.js",null,true).SetVersion("1.18.2");
            manifest.DefineScript("CSSRulePlugin").SetUrl("Greensock/Plugins/CSSRulePlugin.min.js", "Greensock/Plugins/CSSRulePlugin.js").SetCdn("//cdnjs.cloudflare.com/ajax/libs/gsap/1.18.2/plugins/CSSRulePlugin.min.js",null,true).SetVersion("1.18.2");
            manifest.DefineScript("DirectionalRotationPlugin").SetUrl("Greensock/Plugins/DirectionalRotationPlugin.min.js", "Greensock/Plugins/DirectionalRotationPlugin.js").SetCdn("//cdnjs.cloudflare.com/ajax/libs/gsap/1.18.2/plugins/DirectionalRotationPlugin.min.js",null,true).SetVersion("1.18.2");
            manifest.DefineScript("DrawSVGPlugin").SetUrl("Greensock/Plugins/DrawSVGPlugin.min.js", "Greensock/Plugins/DrawSVGPlugin.js").SetVersion("1.18.2");
            manifest.DefineScript("EaselPlugin").SetUrl("Greensock/Plugins/EaselPlugin.min.js", "Greensock/Plugins/EaselPlugin.js").SetCdn("//cdnjs.cloudflare.com/ajax/libs/gsap/1.18.2/plugins/EaselPlugin.min.js",null,true).SetVersion("1.18.2");
            manifest.DefineScript("EndArrayPlugin").SetUrl("Greensock/Plugins/EndArrayPlugin.min.js", "Greensock/Plugins/EndArrayPlugin.js").SetCdn("//cdnjs.cloudflare.com/ajax/libs/gsap/1.18.2/plugins/EndArrayPlugin.min.js",null,true).SetVersion("1.18.2");
            manifest.DefineScript("KineticPlugin").SetUrl("Greensock/Plugins/KineticPlugin.min.js", "Greensock/Plugins/KineticPlugin.js").SetCdn("//cdnjs.cloudflare.com/ajax/libs/gsap/1.18.2/plugins/KineticPlugin.min.js").SetCdn("//cdnjs.cloudflare.com/ajax/libs/gsap/1.18.2/plugins/ColorPropsPlugin.min.js",null,true).SetVersion("1.18.2");
            manifest.DefineScript("MorphSVGPlugin").SetUrl("Greensock/Plugins/MorphSVGPlugin.min.js", "Greensock/Plugins/MorphSVGPlugin.js").SetVersion("1.18.2");
            manifest.DefineScript("Physics2DPlugin").SetUrl("Greensock/Plugins/Physics2DPlugin.min.js", "Greensock/Plugins/Physics2DPlugin.js").SetVersion("1.18.2");
            manifest.DefineScript("PhysicsPropsPlugin").SetUrl("Greensock/Plugins/PhysicsPropsPlugin.min.js", "Greensock/Plugins/PhysicsPropsPlugin.js").SetVersion("1.18.2");
            manifest.DefineScript("RaphaelPlugin").SetUrl("Greensock/Plugins/RaphaelPlugin.min.js", "Greensock/Plugins/RaphaelPlugin.js").SetCdn("//cdnjs.cloudflare.com/ajax/libs/gsap/1.18.2/plugins/RaphaelPlugin.min.js",null,true).SetVersion("1.18.2");
            manifest.DefineScript("RoundPropsPlugin").SetUrl("Greensock/Plugins/RoundPropsPlugin.min.js", "Greensock/Plugins/RoundPropsPlugin.js").SetCdn("//cdnjs.cloudflare.com/ajax/libs/gsap/1.18.2/plugins/RoundPropsPlugin.min.js",null,true).SetVersion("1.18.2");
            manifest.DefineScript("ScrambleTextPlugin").SetUrl("Greensock/Plugins/ScrambleTextPlugin.min.js", "Greensock/Plugins/ScrambleTextPlugin.js").SetVersion("1.18.2");
            manifest.DefineScript("ScrollToPlugin").SetUrl("Greensock/Plugins/ScrollToPlugin.min.js", "Greensock/Plugins/ScrollToPlugin.js").SetCdn("//cdnjs.cloudflare.com/ajax/libs/gsap/1.18.2/plugins/ScrollToPlugin.min.js",null,true).SetVersion("1.18.2");
            manifest.DefineScript("TextPlugin").SetUrl("Greensock/Plugins/TextPlugin.min.js", "Greensock/Plugins/TextPlugin.js").SetCdn("//cdnjs.cloudflare.com/ajax/libs/gsap/1.18.2/plugins/TextPlugin.min.js",null,true).SetVersion("1.18.2");
            manifest.DefineScript("ThrowPropsPlugin").SetUrl("Greensock/Plugins/ThrowPropsPlugin.min.js", "Greensock/Plugins/ThrowPropsPlugin.js").SetVersion("1.18.2");
            manifest.DefineScript("CSSTransform").SetUrl("Greensock/Utils/CSSTransform.min.js").SetVersion("1.18.2");
            manifest.DefineScript("Draggable").SetUrl("Greensock/Utils/Draggable.min.js", "Greensock/Utils/Draggable.js").SetCdn("//cdnjs.cloudflare.com/ajax/libs/gsap/1.18.2/utils/Draggable.min.js",null,true).SetVersion("1.18.2");
            manifest.DefineScript("SplitText").SetUrl("Greensock/Utils/SplitText.min.js", "Greensock/Utils/SplitText.js").SetVersion("1.18.2");
            // Datwendo utilities
            manifest.DefineScript("GSAPTagCloud").SetUrl("GSAPTagCloud.min.js", "GSAPTagCloud.js").SetDependencies("JQuery","TweenMax").SetVersion("1.0.0");
            manifest.DefineScript("GSAPLoopLogo").SetUrl("GSAPLoopLogo.min.js", "GSAPLoopLogo.js").SetDependencies("JQuery","TweenMax").SetVersion("1.0.0");
        }
    }
}

