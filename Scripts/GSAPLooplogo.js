﻿/* GSAPLoopLogo.js v 1.0 - CS - Datwendo 12/2015 */

var Loop = function ()
{
    TweenLite.killTweensOf('.logo');
    TweenLite.set('.logo', { clearProps: 'all' });
    TweenMax.from('.logo', 3, { rotation: 360, rotationX: 720, x: -1000, scale: 0, opacity: 0 });
};

$(function () {
    TweenLite.set(containerImg, { perspective: 500 });
    Loop();
});

var globalResizeTimer = null;

$(window).resize(function () {
    if (globalResizeTimer != null) window.clearTimeout(globalResizeTimer);
    globalResizeTimer = window.setTimeout(function () {
        Loop();
    }, 200);
});