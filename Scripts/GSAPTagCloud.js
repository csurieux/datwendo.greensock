﻿/* GSAPTagCloud.js v 1.0 - CS - Datwendo 12/2015 */

$(document).ready(function () {
        var MR = function (X) { return Math.random() * X },
            TwL = TweenLite,
            G = $('.tag-cloud-tag');
        var tags = $(".tag-cloud").first();
        var W = tags.innerWidth(),
                H = tags.innerHeight(),
                C = 40,
                SPs = 1,
                nbElem = G.length;
        function BTweens() {
            TwL.killDelayedCallsTo(BTweens);
            TwL.delayedCall(C * 4, BTweens);
            for (var i = nbElem; i--;) {
                var c = C,
                    BA = [],
                    GWidth = G[i].offsetWidth,
                    GHeight = G[i].offsetHeight;
                while (c--) {
                    var SO = MR(1);
                    BA.push({ x: MR(W - GWidth), y: MR(H - GHeight), scale: SO, opacity: SO, zIndex: Math.round(SO * 7) });
                };
                if (G[i].T) { G[i].T.kill() };
                G[i].T = TweenMax.to(G[i], C * 4, { bezier: { timeResolution: 0, type: "soft", values: BA }, delay: i * 0.35, ease: Linear.easeNone });
            }
        };

        function Play() {
            if (SPs) { for (var i = nbElem; i--;) { if (G[i].T) { G[i].T.pause(); } } SPs = 0; }
            else { for (var i = nbElem; i--;) { if (G[i].T) { G[i].T.play(); }} SPs = 1; }
        };

        tags.on('mouseenter', Play);
        tags.on('mouseleave', Play);

        BTweens();
        window.onresize = function () {
            TwL.killDelayedCallsTo(BTweens); TwL.delayedCall(0.4, BTweens);
        };
    });
